*******
Install
*******

Requirements
============

* podman

Install
=======

.. code-block:: bash

  $ sudo pip install molecule molecule-plugins[podman]
